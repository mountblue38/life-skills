# Grit and Growth Mindset

## 1. Grit

### Question 1

Paraphrase (summarize) the video in a few lines. Use your own words.

### Answer

In the video, angela talks about grit, when she made research on who is successful, who is going to earn more money, and who is going to survive in their job she got to know that grit is the key thing to all of that. Grit is passion and perseverance for very long-term goals. Grit is having stamina. Grit is sticking with your future, we should stick around to specific jobs not just for the week, not just for the month, but for years, and working really hard to make that future good. Grit is like a marathon we should keep on running.

### Question 2

What are your key takeaways from the video to take action on?

### Answer

Grit is very important in life. Grit is passion and perseverance for very long-term goals. If we are doing something or a specific job consistency and a grit mindset matter a lot we should not lose hope in thinking that we can't do it and should stick on to it and try to complete it gradually because everything takes time 

## 2. Introduction to Growth Mindset

### Question 3

Paraphrase (summarize) the video in a few lines in your own words.

### Answer

Here the author says that a growth mindset is a foundation for great learning, It's the fact that you can do even better than your current version

Mainly there will be two types of mindset
1. Growth mindset 
2. Fixed mindset

People with a fixed mindset think that skills are born and we can't learn them and grow whereas people with a growth mindset think that they are built and can learn and grow

### Question 4

What are your key takeaways from the video to take action on?

### Answer

A person should have a growth mindset and if he slips into the fixed mindset he should recognize it and change it to the growth mindset
 
A person who has a fixed mindset will be less focused, Regret mistakes, and takes the comments from the parents and mentors inappropriately. A person with a growth mindset will always be focused, learn and grow from their mistake and takes guidance from their parents and mentors to optimize themself. a person with a fixed mindset should start pointing out things and should always try to overcome them and should start adopting the growth mindset

## 3. Understanding Internal Locus of Control

### Question 5

What is the Internal Locus of Control? What is the key point in the video?

### Answer

Internal locus of control is the degree to which you believe you have control over life. The amount of control u have over your emotions and mind

we need to be extremely motivated to face the easy or the difficult things in life. To always keep ourselves motivated we should keep on appreciating our work and should think positively

## 4. How to build a Growth Mindset

### Question 6

Paraphrase (summarize) the video in a few lines in your own words.

### Answer

Here Brendon talks about how to develop a growth mindset. The first and major thing is belief and all the person should believe in their ability. one should not think that I'm not capable instead figure out how things work and start learning things and make it possible and should question your negative thoughts and become positive about it.

### Question 7

What are your key takeaways from the video to take action on?

### Answer

The main key takeaways are we should have belief in ourselves and should always question ourselves and stop thinking negatively and always try to be positive. If there are any difficulties take time and overcome them no matter what. Should honor our work whenever possible

## 5. Mindset - A MountBlue Warrior Reference Manual

### Question 8

What are one or more points that you want to take action on from the manual? (Maximum 3)

### Answer

1. I will stay relaxed and focused no matter what happens.
2. I will not leave my code unfinished till I Make it readable, Make it modular and Make it efficient
3. I will have a smile on my face when I face new challenges
