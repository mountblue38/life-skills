## 1. How to Learn Faster with the Feynman Technique

### What is the Feynman Technique?

This technique is named after physicist Feynman. This technique tells that explaining a concept to others that are sharing your knowledge improves your understanding. It is also used to test your understanding and challenge your assumptions.

### What are the different ways to implement this technique in your learning process?

1. Get a piece of paper and write the concept's name at the top
2. explain the concept using simple language
3. Identify problem areas, then go back to sources to review
4. pinpoint any complicated terms and challenge yourself to simplify them

## 2. Learning How to Learn TED talk by Barbara Oakley

### Paraphrase the video in detail in your own words

There will be 2 modes while learning namely
1. Focus mode
2. Diffuse mode
When we will be learning we will go back and forth between the modes.
For example when focusing on something in solving a tricky question if we are stuck our mind tries to get away from that problem and gets into a diffuse state.
Whenever we are sitting down to solve a new problem or analyze a new idea, even if many people thought the same thoughts or solved the same problem for us it is as creative as it was for famous people like Edison and you have to use some of the creative approaches.
Don't just follow your passions,
BROADEN your passions,
and your life will be enriched beyond measure

### What are some of the steps that you can take to improve your learning process?

* exercise- One of the ways to improve the learning process is through exercise. It helps to learn and remember
* Test-Test yourself all the time, give the mini test to yourself, make flashcards
* Recall - Recall what you learned. It helps to profound neural hooks that help in enhancing your understanding of the material

## 3. Learn Anything in 20 hours

### Your key takeaways from the video? Paraphrase your understanding.

It takes 10,000 hrs to get to the top of an ultra-competitive field in a very narrow subject but not learn something new. Read a little, then practice a little, then progress that way. People get good at things with little practice. If you put 20 hrs of focused deliberate practice into a thing you will be good at it. The main barrier is not intellectual - it's emotional

### What are some of the steps that you can while approaching a new topic?

1. Deconstruct the skill - Practice the most important thing first in less amount of time than possible
2. Learn enough to self-correct - get 3 to 5 resources of what it is you are trying to learn
3. Remove Practice barriers - Distractions, television, and the internet will kind of act like a barrier.
4. Practice at least 20 hours - This helps to overcome the intial frustration barrier and stick with the practice long enough to leap the rewards.
