# SOA (Service Oriented Architecture)

## What is Service-oriented Architecture

A Service-Oriented Architecture or SOA is a pattern of working which is designed to create distributed systems that deliver services to other applications through the protocol. It is only a concept and not limited to any programming language or platform.

* SOA is helpful to scale to meet the speed and operational demands of big data. To break large, complex applications into small, flexible components that are easier to work with. And differ from traditional, monolithic architecture in that every service has its responsibility.

* SOA is related to the idea of an API (application programming interface), an interface or communication protocol between different parts of a computer program intended to simplify the implementation and maintenance of software. 

* SOA will be having some set of rules that structures system development and makes integrating components easy.

There are two roles in Service Oriented Architecture they are:
1. Service provider
2. Service Consumer

The Service Provider is the one who provides the service or who maintains the service

Service Consumer can be called the Requestor or client who calls the service provider

![SOA Service provider and consumer](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-245.png)

The above image represents the connection between the Service consumer and the Service provider Service consumer sends a service request to the service provider, and the service provider sends the service response to the service consumer. The service connection is understandable to both the service consumer and the service provider.

#### Components of Service-oriented Architecture

![Components of SOA](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-248.png)

## Advantages of SOA: 

1. Easy maintenance: As services are independent of each other they can be updated and modified easily without affecting other services.
2. Reliability: SOA applications are more reliable because it is easy to debug small services rather than huge codes.  
3. Service reusability: In Service Oriented Architecture, applications are made from existing services. Thus, services can be reused many times to create new applications.   
4. Parallel Development: Service Oriented Architecture supports parallel development, where services can be developed parallelly as there will be less dependency on the other services.
5. Platform independent: SOA allows making a complex application by combining services picked from different sources, independent of the platform.   
6. Scalability: Services can run on different servers within an environment, this increases scalability.  
7. Availability: SOA facilities are easily available to anyone on request.    
8. Loose coupling: It facilitates implementation services without impacting other applications or services.   
9. Easy to test:  With the help of Service-oriented Architecture testing of applications is made easy.


## Implementation approaches: 

* Service-oriented architecture can be implemented with web services or Microservices.

* Service-Oriented Architecture is also able to work around firewalls, allowing companies to share services that are vital to operations.

* SOA is used in many ways around us whether it is mentioned or not. for a practical example SOA infrastructure is used by many armies and air forces to deploy situational awareness systems, an app might need GPS so it uses the inbuilt GPS functions of the device.
    
* An example of a web service standard is SOAP, which stands for Simple Object Access Protocol. In a nutshell, SOAP “is a messaging protocol specification for exchanging structured information in the implementation of web services in computer networks.

 ## References
 
 * https://en.wikipedia.org/wiki/Service-oriented_architecture
 * https://www.geeksforgeeks.org/service-oriented-architecture/
 * https://www.javatpoint.com/service-oriented-architecture
