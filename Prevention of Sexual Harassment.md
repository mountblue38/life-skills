## Prevention of Sexual Harassment

### 1. What kinds of behavior cause sexual harassment?

Sexual harassment is any unwelcome, improper, visual, or verbal of sexual nature which affects the work nature.
there are three forms of sexual harassment 
1. Verbal - Speaking bad words with a coworker or commenting on their clothes, body, behavior, or anything that hurts their feeling. Spreading rumors about a person's personal life is one of the major sexual harassment.
2. Visual - posting personal pictures, drawing unnecessary bad pictures, or writing mail or text which denotes sexual harassment.
3. Physical harassment - Physical harassment includes sexual assault, blocking movement or inappropriate movement with the worker, and Inappropriate touching which hurts his/her feeling. 

### 2. What would you do in case you face or witness any incident or repeated incidents of such behavior?

It always depends on the situation, If I see any such behavior intend to be sexual harassment I will immediately inform the higher authority and try to keep the recording of it as proof.
