# What are the steps/strategies to do Active Listening?

## 1. Face the speaker and have eye contact
Eye contact is an important part of face to face conversation. Too much eye contact can be intimidating, though, so adapt this to the situation you’re in. Try breaking eye contact every five seconds or so, or to show you’re listening attentively, look at one eye for five seconds, then another eye for five seconds, then switch to looking at their mouth.

## 2. Listen without judging, or jumping to conclusions
If you start reacting emotionally to what’s being said, then it can get in the way of listening to what is said next. Try to focus on listening. Equally, don’t assume that you know what’s going to be said next.

## 3.Don’t start planning what to say next
You can’t listen and prepare at the same time.

## 4. Stay focused

If you're finding it difficult to focus on what someone is saying, try repeating their words in your head as they say them – this will reinforce what they’re saying and help you to concentrate. Try to shut out distractions like other conversations going on in the room

## 5. Paraphrase and summarise
Sometimes called reflecting, this is repeating what has been said to show that you understand it. This may seem awkward at first, but really shows you’ve been paying attention, and allows the speaker to correct you if you haven’t understood correctly.

## 6. “Listen” to non-verbal cues too

Facial expressions, tone of voice and gestures can tell you just as much as what is being said in words. Pay attention to what the other person is saying with their body language - are they smiling, for example, or are their arms crossed defensively, or are they rubbing their eyes as if they're tired or upset. 

# According to Fisher's model, what are the key points of Reflective Listening? 

Dalmar Fisher gave a model for Reflective Communication that talks about the listed elements:-

*  Avoidig the distraction and focusing on the conversation.

* Having a non-judgemental perspective to the speakers point, the listner can embrace the speakers even if they do not readily relate to it.

* The listner should try to mirror the mood of the speaker.

* Summarizing the content of the speaker.

* Responding to the speaker's specific point, without digressing to other subject

# What are the obstacles in your listening process?

### Listening Barriers
Unfortunately, effective listening can be held back by barriers. These barriers to listening can be grouped into two major categories: external and internal.

### External Listening Barriers
External listening barriers are easier to manage than internal barriers. They include a variety of environmental distractions that can usually be avoided or minimized with simple corrections, like removing yourself from the interfering barrier or removing the issue from the area that you are in. External barriers include:

* ## Noise.
Any external noise can be a barrier, like the sound of equipment running, phones ringing, or other people having conversations.
* ## Visual distractions. 
Visual distractions can be as simple as the scene outside a window or the goings-on just beyond the glass walls of a nearby office.
* ## Physical setting. 
An uncomfortable temperature, poor or nonexistent seating, bad odors, or distance between the listener and speaker can be an issue.
* ## Objects. 
Items like pocket change, pens, and jewelry are often fidgeted with while listening.
The person speaking. The person listening may become distracted by the other person’s personal appearance, mannerisms, voice, or gestures.
* # Internal Listening Barriers
Internal listening barriers are more difficult to manage, as they reside inside the mind of the listener. Internal barriers’ elimination relies on a high level of self-awareness and discipline on the part of the listener, like catching oneself before the mind starts to wander and bringing full attention back to the speaker. Internal barriers include:

* ## Anxiety. 
Anxiety can take place from competing personal worries and concerns.
* ## Self-centeredness. 
This causes the listener to focus on his or her own thoughts rather than the speaker’s words.
* ## Mental laziness. 
Laziness creates an unwillingness to listen to complex or detailed information.
Boredom. Boredom stems from a lack of interest in the speaker’s subject matter.
Sense of superiority. This leads the listener to believe they have nothing to learn from the speaker. 

# When do you switch to Passive communication style in your day to day life?

passive communication is an ineffective way of getting one’s views across. Passive communicators rarely express their needs, thoughts or feelings in public. Instead, they hide their emotions and let others ‘walk over them’. While a passive communication style may seem harmless, it’s not. Because it makes people believe that such communicators are powerless, pushovers or uninterested, when that may be far from the truth.

And in my case I use passive communication when I did not like something and want to tell the other person without offending him. Then i use the passive communuication.

## 6. When do you switch into Aggressive communication styles in your day to day life?


When someone annoys me during my work. And keep poking me with some nonsense question or something.

## 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

Generally, I, switch to passive agressive/ sarcasm when I am with my friends.

## 8. How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

Following steps can be taken to make the communication assertive :-

Choose the right time, one should choose the right time to bring up an issue.

Be direct. Be direct in your conversation, do not beat around the bush.

Say “I,” not “we.”

Use body language to emphasize your words.

Stand up for yourself. If you are not wrong, do not doubt yourself and beat yourself down.
